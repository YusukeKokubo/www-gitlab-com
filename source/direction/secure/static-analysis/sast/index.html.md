---
layout: markdown_page
title: "Category Direction - Static Applicaton Security Testing (SAST)"
---

- TOC
{:toc}

## Description

### Overview

Static application security testing (SAST) checks the source code to find possible vulnerabilities in the implementation.

It can analyze the control flow, the abstract syntax tree, how functions are invoked, and if there are information leaks in order to detect weak points that may lead to unintended behaviors.

The nature of SAST is very language specific. Since each language has its own syntax and features, SAST requires specific analyzers to target different languages, like Java, Node.js, and Ruby.

### Goal

Our goal is to provide SAST as part of the standard development process. This means that SAST is executed every time a new commit is pushed to a branch. We also include SAST as part of [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

We also want to make SAST complexity totally transparent to users. GitLab is able to automatically detect the programming language and to run the proper analyzer. We want to increase language coverage by including support for the most common languages.

SAST results can be consumed in the merge request, where only new vulnerabilities, introduced by the new code, are shown. A full report is available in the pipeline details page.

SAST results are also part of the [Security Dashboard](https://docs.gitlab.com/ee/user/application_security/security_dashboard/), where Security Teams can check the security status.
### Roadmap

- First MVC (already shipped): https://docs.gitlab.com/ee/user/application_security/sast/
- Static Application Security Testing (SAST) improvements: https://gitlab.com/groups/gitlab-org/-/epics/297

## What's Next & Why

We want to include more common languages, so that the majority of applications could benefit of SAST.

The next MVC is to support Elixir: https://gitlab.com/gitlab-org/gitlab-ee/issues/9399

## Maturity Plan
 - [Base Epic](https://gitlab.com/groups/gitlab-org/-/epics/527)
 
## Competitive Landscape

There are many well-known commercial products that are providing SAST. Most of them support multiple languages, and are integrated into the development lifecycle.

Here are some vendors providing SAST tools:
- Checkmarx: https://www.checkmarx.com/products/static-application-security-testing/
- Synopsis: https://www.synopsys.com/software-integrity/security-testing/static-analysis-sast.html
- CA Veracode: https://www.veracode.com/products/binary-static-analysis-sast
- Fortify: https://software.microfocus.com/en-us/products/static-code-analysis-sast/overview
- IBM AppScan: https://www.ibm.com/security/application-security/appscan
- SonarQube: https://www.sonarqube.org/

GitLab has the unique position to deeply integrate into the development lifecycle, with the ability to leverage CI/CD pipelines to perform the security tests. There is no need to connect the remote source code repository, or to use a different interface.

Anyway, we can improve the experience even more, by supporting additional features that are currently present in other tools.

- Support incremental scans for SAST: https://gitlab.com/gitlab-org/gitlab-ee/issues/9815
- Auto Remediation support for SAST: https://gitlab.com/gitlab-org/gitlab-ee/issues/9480

## Analyst Landscape

We want to engage analysts to make them aware of the security features already available in GitLab. Since this is a relatively new scope for us, we must aim at being included in the next researches.

We can get valuable feedback from analysts, and use it to drive our vision.

- https://www.gartner.com/reviews/market/application-security-testing
- https://www.forrester.com/report/Predictions+2019+Cybersecurity/-/E-RES144821

We can improve the visibility with analysts by ensuring that they can easily compare to other tools from a results point of view (https://gitlab.com/gitlab-org/gitlab-ee/issues/9816).

[Full list](https://gitlab.com/groups/gitlab-org/-/issues?state=opened&sort=milestone&label_name[]=analysts&label_name[]=sast)

## Top Customer Success/Sales Issue(s)

- https://gitlab.com/gitlab-org/gitlab-ee/issues/7271
- https://gitlab.com/gitlab-org/gitlab-ee/issues/7158

[Full list](https://gitlab.com/groups/gitlab-org/-/issues?state=opened&sort=milestone&label_name[]=customer&label_name[]=sast)

## Top user issue(s)

- https://gitlab.com/gitlab-org/gitlab-ee/issues/6711
- https://gitlab.com/gitlab-org/gitlab-ee/issues/7271

[Full list](https://gitlab.com/groups/gitlab-org/-/issues?state=opened&sort=popularity&label_name[]=sast)

## Top internal customer issue(s)

- https://gitlab.com/gitlab-org/gitlab-ee/issues/9324

[Full list](https://gitlab.com/groups/gitlab-org/-/issues?state=opened&sort=milestone&label_name[]=internal%20customer&label_name[]=sast)

## Top Vision Item(s)

- https://gitlab.com/gitlab-org/gitlab-ee/issues/7271
- https://gitlab.com/groups/gitlab-org/-/epics/568
