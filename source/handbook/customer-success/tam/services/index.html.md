---
layout: handbook-page-toc
title: "TAM Responsibilities and Services"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [TAM Handbook homepage](/handbook/customer-success/tam/) for additional TAM-related handbook pages.

---

## Responsibilities and Services

All Premium Enterprise customers with a minimum Annual Recurring Revenue (ARR) of $100,000 are aligned with a Technical Account Manager.

Commercial (Mid-Market and SMB) customers are eligible for a TAM based on varying factors, such as tier, ARR, and potential for growth. The TAM will work with their aligned Account Executive to determine which accounts to prioritize.

There are various services a Technical Account Manager will provide to ensure that customers get the best value possible out of their relationship with GitLab and their utilization of GitLab's products and services. These services typically include the following. Please note this list is not definitive, and more services may be provided than listed or some may not be offered, depending on the size and details of the account.

### Relationship Management
* Regular cadence calls
    * Ask customers about planned upgrades on a regular basis. Refer them to the [Live Upgrade Assistance page](/support/scheduling-live-upgrade-assistance.html#how-do-i-schedule-live-upgrade-assistance) so that they have a plan in place (including rollback strategy) and give Support enough preparation time to be available to help.
* Regular open issue reviews and issue escalations
* Account health checks
* Executive business reviews (1-2 times a year for Enterprise; as needed for Commercial)
* Success strategy roadmaps - beginning with an onboarding success plan, for example a 30/60/90 day plan
* To act as a key point of contact for guidance and advice and as a liaison between the customer and other GitLab teams
* Own, manage, and deliver the customer onboarding experience
* Help GitLab's customers realize the value of their investment in GitLab
* GitLab Days

### Training
* Identification of pain points and training required
* Coordination of demos and training sessions, potentially delivered by the Technical Account Manager if time and technical knowledge allows
* "Brown Bag" trainings
* Regular communication and updates on GitLab features
* Product and feature guidance - new feature presentations

### Support
* Upgrade planning
* User adoption strategy
* Migration strategy and planning
* Launch support
* Monitors support tickets and ensures that the customer receives the appropriate support levels
* Support ticket escalations

It is also possible for a customer to pay for a Technical Account Manager's services in order to receive priority, "white glove" assistance, guidance and support as well as more time allocated to their account on a monthly basis. There are also additional services a Technical Account Manager will provide to the services listed above.
