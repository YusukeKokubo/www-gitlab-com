---
layout: markdown_page
title: "Usecase: Continuous Integration"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


## Continuous Integration

The Continuous Integration (CI) usecase is a staple of modern software development in the digital age. It's unlikely that you hear the word "DevOps" without a reference to "Continuous Integration and Continuous Delivery" (CI/CD) soon after. In the most basic sense, the CI part of the equation enables development teams to automate building and testing their code.

When practicing CI, teams collaborate on projects by using a shared repository to store, modify and track frequent changes to their codebase. Developers check in, or integrate, their code into the repository multiple times a day and rely on automated tests to run in the background. These automated tests verify the changes by checking for potential bugs and security vulnerabilities, as well as performance and code quality degradations. Running tests as early in the software development lifecycle as possible is advantageous in order to detect problems before they intensify.

CI makes software development easier, faster, and less risky for developers. By automating builds and tests, developers can make smaller changes and commit them with confidence. They get earlier feedback on their code in order to iterate and improve it quickly increasing the overall pace of innovation. Studies done by DevOps Research and Assessment (DORA) have shown that [robust DevOps practices lead to improved business outcomes](https://cloud.google.com/devops/state-of-devops/). Out of the "DORA 4" metrics, 3 of them can be improved by using CI:
- **Deployment frequency:** Automated build and test is a pre-requisite to automated deploy.
- **Time to restore service:** Automated pipelines enable fixes to be deployed to production faster reducing Mean Time to Resolution (MTTR)
- **Change failure rate:** Early automated testing greatly reduced the number of defects that make their way out to production.

[GitLab CI](/stages-devops-lifecycle/continuous-integration/) comes built-in to GitLab's complete DevOps platform delivered as a single application. There's no need to cobble together multiple tools and users get a seamless experience out-of-the-box.

## Personas

### User Persona

The typical **user personas** for this usecase are the Developer, Development team lead, and DevOps engineer.

#### Software Developer [Sacha](/handbook/marketing/product-marketing/roles-personas/#sasha-software-developer)

Software developers have expertise in all sorts of development tools and programming languages, making them an extremely valuable resource in the DevOps space. They take advantage of GitLab SCM and GitLab CI together to work fast and consistently deliver high quality code.

- Developers are problem solvers, critical thinkers, and love to learn. They work best on planned tasks and want to spend a majority of their time writing code that ends up being delivered to customers in the form of lovable features.

#### Development Team Lead [Delaney](/handbook/marketing/product-marketing/roles-personas/#delaney-development-team-lead)

Development team leads care about their team's productivity and ability to deliver planned work on time. Leveraging GitLab CI helps maximize their team's productivity and minimize disruptions to planned tasks.

- Team Leads need to understand their team's capacity to assign upcoming tasks, as well as help resolve any blockers by assigning to right resources to assist.

#### DevOps Engineer [Devon](/handbook/marketing/product-marketing/roles-personas/#devon-devops-engineer)

DevOps Engineers have a deep understanding of their organization's SDLC and provide support for infrastructure, environments, and integrations. GitLab CI makes their life easier by providing a single place to run automated tests and verify code changes integrated back into the SCM by development teams.

- DevOps Engineers directly support the development teams and prefer to work proactively instead of reactively. They split their time between coding to implement features and bug fixes, and helping developers build, test, and release code.

### Buyer Personas

CI purchasing typically does not require executive involvement. It is usually acquired and installed via our freemium offering without procurement or IT's approval. This process is commonly known as shadow IT. When the upgrade is required, the [Application Development Manager](/handbook/marketing/product-marketing/roles-personas/buyer-persona/#alex---the-application-development-manager) is the most frequent decision-maker. The influence of the [Application Development Director](/handbook/marketing/product-marketing/roles-personas/buyer-persona/#dakota---the-application-development-director) is notable too.  

## Market Requirements

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-1wig{font-weight:bold;text-align:left;vertical-align:top}
.tg .tg-fymr{font-weight:bold;border-color:inherit;text-align:left;vertical-align:top}
.tg .tg-0lax{text-align:left;vertical-align:top}
</style>
<table class="tg">
  <tr>
    <td class="tg-fymr">Market Requirement</td>
    <td class="tg-fymr">Description</td>
    <td class="tg-fymr">Typical capability-enabling features</td>
    <td class="tg-fymr">Value/ROI</td>
  </tr>
  <tr>
    <td class="tg-1wig">1) Build and test automation</td>
    <td class="tg-0lax">Streamlines application development workflow by connecting simple, repeatable, automated tasks into a series of interdependent automatic builds and associated tests. Run and manage automated tasks in the background, preview and validate changes before it's merged to production. Ensure software is consistently built and tested without manual intervention, enabling developers to get rapid feedback if their code changes introduce defects or vulnerabilities. Teams have control over where automated jobs run either in the cloud (public/private/hybrid) or using shared infrastructure.</td>
    <td class="tg-0lax">CI/CD pipelines, scalable resources, job orchestration/work distribution, automated deployments, caching, external repository integrations, and the ability to run isolated, automated, tests (such as unit tests, regression tests, etc.).</td>
    <td class="tg-0lax">Development teams can work with speed and efficiency. Catch potential errors sooner rather than later before they intensify.</td>
  </tr>
  <tr>
    <td class="tg-1wig">2) Configuration management</td>
    <td class="tg-0lax">The CI solution makes it easy for developers to automate the build and test workflow, specifically connecting to their source code repository, defining specific actions/tasks in build pipelines, and set parameters for exactly how/when to run jobs, using scripts and/or a GUI to configure pipeline changes. Configurations are easily reproducible and traceability exists to allow for quick comparisons and tracking of changes to environments.</td>
    <td class="tg-0lax">Configurations via web UI or supports config-as-code in a human readable syntax, like YAML.</td>
    <td class="tg-0lax">Maximize development time and improves productivity. Less manual work.</td>
  </tr>
  <tr>
    <td class="tg-1wig">3) Easy to get started</td>
    <td class="tg-0lax">The steps required to successfully setup CI should be simple and straightforward with minimal barrier to entry. The time and effort required for teams to get started from initial installation,&nbsp;&nbsp;configuration, onboarding users, and delivering value should be short (days, not weeks).</td>
    <td class="tg-0lax">Supports both on-premise and SaaS implementations. Has robust documentation outlining steps to connect to a repository and get a CI server up and running quickly. Supports configuration-as-code or provides a GUI for initial configurations. Web interface to provision users.</td>
    <td class="tg-0lax">Faster time to value.</td>
  </tr>
  <tr>
    <td class="tg-1wig">4) Platform and language support</td>
    <td class="tg-0lax">Many organizations use a mix of operating systems and code stacks within their development teams.&nbsp;&nbsp;The CI solution must operate on multiple operating systems (Windows, Linux, and Mac). The CI solution must also be language-agnostic supporting automation of build and test for a wide variety of development languages(Java, PHP, Ruby, C, etc.).</td>
    <td class="tg-0lax">Option to use native packages, installers, downloads from source, and containers. Cloud-native platform support for public, private, or hybrid hosting. Runs on any OS.</td>
    <td class="tg-0lax">Gives teams more flexibility, making it easier to adopt.</td>
  </tr>
  <tr>
    <td class="tg-1wig">5) Visibility and collaboration</td>
    <td class="tg-0lax">The solution enables development teams to have visibility into pipeline structure, specific job execution, and manage changes and updates to the pipeline jobs. The solution should enable teams to easily collaborate and make code changes, review work, and communicate directly. CI solutions should provide visibility and insights into the state of any build.</td>
    <td class="tg-0lax">Pull requests or merge requests, commit history, automatic notifications/alerts, chatOps, code reviews, preview environments.</td>
    <td class="tg-0lax">Faster feedback and less build breaking changes.</td>
  </tr>
  <tr>
    <td class="tg-1wig">6) DevOps tools integrations</td>
    <td class="tg-0lax">The solution supports strong integrations to both upstream and downstream processes such as project management, source code management and version control, artifact repositories, security scanning, compliance management, continuous delivery, etc. A CI solution with strong DevOps integrations means there's flexibility for users who need or want a balance between native capabilities and integrations.</td>
    <td class="tg-0lax">Integrations with build automation tools (Gradle, Maven etc.), code analysis tools, binary repos, CD tools, IDEs, APIs, third party libraries or extensibility via plugins.</td>
    <td class="tg-0lax">Increases efficiency. Lessens cost and the extra work that comes along with potential migrations.</td>
  </tr>
  <tr>
    <td class="tg-1wig">7) Pipeline security</td>
    <td class="tg-0lax">Access to the CI pipeline is managed and controlled in order to facilitate secure access, safely store/manage sensitive data (such as secrets), and ensure compliance in the event of an audit.</td>
    <td class="tg-0lax">Access control (like RBAC), enterprise-based access systems (like LDAP), automated security scans and tests, data encryption, secrets management, and secure network connections.</td>
    <td class="tg-0lax">Reduces business risk and protects intellectual property. Instills confidence in end-users.</td>
  </tr>
  <tr>
    <td class="tg-1wig">8) Analytics</td>
    <td class="tg-0lax">Capture and report on pipeline performance metrics and data throughout the build/test processes in order to identify bottlenecks, constraints, and other opportunities for improvement. Identify failing tests, code quality issues, and valuable trends in data indicative of overall application quality.</td>
    <td class="tg-0lax">Build health, pipeline visualization, code coverage, logging, and performance metrics such as deployment frequency.</td>
    <td class="tg-0lax">Increase efficiencies and reduce unnecessary costs.</td>
  </tr>
  <tr>
    <td class="tg-1wig">9) Elastic scalability</td>
    <td class="tg-0lax">The solution supports elastic scaling leveraging well understood, third-party, mechanisms whenever possible. Supports build scalability around existing native per-cloud and per-virtual environment vendor capabilities since they represent already debugged code for a complex problem, and the customers architects/operators are much more likely to be familiar with these mechanisms in their chosen infrastructure/cloud provider.</td>
    <td class="tg-0lax">Utilizes strong Kubernetes integration at-scale or supports non-Kubernetes scaling options. For example, use AWS Autoscaling Groups in AWS, GCP scaling in GCP, and Azure Autoscale in Azure.</td>
    <td class="tg-0lax">Reduce infrastructure overhead with on-demand resources. Provides flexibility to scale large workloads using popular cloud providers.</td>
  </tr>
  <tr>
    <td class="tg-1wig">tbd</td>
    <td class="tg-0lax"></td>
    <td class="tg-0lax"></td>
    <td class="tg-0lax"></td>
  </tr>
</table>

## Top GitLab Features for CI

* **Multi-platform:** you can execute builds on Unix, Windows, OSX, and any other platform that supports Go.
* **Multi-language:** build scripts are command line driven and work with Java, PHP, Ruby, C, and any other language.
* **Parallel builds:** GitLab splits builds over multiple machines for fast execution.
* **Autoscaling:** you can automatically spin up and down VM's or Kubernetes pods to make sure your builds get processed immediately while minimizing costs.
* **Realtime logging:** a link in the merge request takes you to the current build log that updates dynamically.
* **Versioned tests:** a .gitlab-ci.yml file that contains your tests, allowing developers to contribute changes and ensuring every branch gets the tests it needs.
* **Flexible Pipelines:** define multiple jobs per stage and even trigger other pipelines.
* **Build artifacts:** upload binaries and other build artifacts to GitLab. Easily browse and download them.
* **Test locally:** reproduce tests locally using `gitlab-runner exec`.
* **Docker support:** use custom Docker images, spin up services as part of testing, build new Docker images, run on Kubernetes.
* **Container Registry:** built-in container registry to store, share, and use container images.
* *we'll add to this list as we continue to build out more lovable features!*

## Top 3 GitLab Differentiators

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-1wig{font-weight:bold;text-align:left;vertical-align:top}
.tg .tg-fymr{font-weight:bold;border-color:inherit;text-align:left;vertical-align:top}
.tg .tg-0lax{text-align:left;vertical-align:top}
</style>
<table class="tg">
  <tr>
    <th class="tg-fymr">Differentiator</th>
    <th class="tg-fymr">Value</th>
    <th class="tg-fymr">Proof Point</th>
  </tr>
  <tr>
    <td class="tg-1wig">1) Leading SCM and CI in one application</td>
    <td class="tg-0lax">GitLab enables streamlined code reviews and collaboration at proven enterprise scale, making development workflows easier to manage and minimizing context switching required between tools in complex DevOps toolchains. Users can release software faster and outpace the competition with the ability to quickly respond to changes in the market.</td>
    <td class="tg-0lax"> Forrester names GitLab among the leaders in <a href="https://about.gitlab.com/analysts/forrester-ci/">Continuous Integration Tools in 2017</a>, Alteryx uses GitLab to have code reviews, source control, CI, and CD <a href="https://about.gitlab.com/customers/alteryx/">all tied together</a>.</td>
  </tr>
  <tr>
    <td class="tg-1wig">2) Rapid innovation</td>
    <td class="tg-0lax">GitLab embraces an approach to rapid innovation that organizations undergoing digital transformations or adopting CI/CD also work diligently to implement internally with frequent, regular, releases delivered on a monthly basis. This provides end users with the latest and greatest in terms of capabilities/features, consistent security updates, and other incremental value adds over time. By GitLab "walking the talk" in regards to CI/CD, we understand the needs and pains that our users and organizations using GitLab face and share a mutual benefit from fully embracing this model centered around continuous improvement.</td>
    <td class="tg-0lax"> GitLab deploys over 160 times a day and is one of the <a href="https://about.gitlab.com/blog/2017/07/06/gitlab-top-30-highest-velocity-open-source/">30 Highest Velocity Open Source Projects</a> from the CNCF, we're voted as a <a href="https://about.gitlab.com/is-it-any-good/#gitlab-has-been-voted-as-g2-crowd-leader">G2 Crowd Leader 2018</a> with more than 170 public reviews and a 4.4 rating noting "Powerful team collaboration for managing software development projects," and have over 2,900 active contributors.</td>
  </tr>
  <tr>
    <td class="tg-1wig">3) Built in security and compliance</td>
    <td class="tg-0lax">GitLab comes with security features out-of-the-box and automated security testing with audit controls to facilitate policy compliance. Moving security testing farther left into the SDLC catches potential problems earlier and shortens feedback loops for developers. This means a faster time to market delivering secure, compliant, code and an increase in customer confidence.</td>
    <td class="tg-0lax"> Gartner mentions GitLab as a vendor in the Application Monitoring and Protection profile in its <a href="https://www.gartner.com/en/documents/3953770/hype-cycle-for-application-security-2019">2019 Hype Cycle for Application Security</a>. Wag! takes advantage of <a href="https://about.gitlab.com/blog/2019/01/16/wag-labs-blog-post/">built-in security and faster releases with GitLab</a>, and Glympse makes their <a href="https://about.gitlab.com/customers/glympse/">audit process easier and remediates security issues faster</a>.</td>
  </tr>
</table>

## [Message House](./message-house/)

The message house provides a structure to describe and discuss the value and differentiators for Continuous Integration with GitLab.

## Customer Facing Slides

<figure class="video_container">
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRmT3xh0VnNckhZOKRJz1x02tfY90ySaYb48YM55HInYMWa8fmSugK6lknvTChiNWSyYgy4ngK9FK3B/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
</figure>

### Discovery Questions

The sample discovery questions below are meant to provide a baseline and help you uncover opportunities when speaking with prospects or customers who are not currently using GitLab for CI. See a more complete list of questions, provide feedback, or comment with suggestions for [GitLab CI discovery questions](https://docs.google.com/document/d/12NJZZr4A_CQWlODNc2JMWc1Gqtt2_uelPYLdT-VAm-M/edit?usp=sharing) and feel free to contribute!

**Sample questions**

* CI/CD tool sprawl is one of the most common problems we see. How do you manage the complexities of many different teams using various tools and still meet their needs?
* Has there been any discussion to standardize on a single solution for CI since you’re already using GitLab for other needs?
* How are you currently supporting CI internally? Do you have a dedicated team or require in-house expertise for guidance, best practices, and fixing issues?
* How often is your day to day or planned work interrupted to fix or maintain your CI tool?
* Some of the teams I talk with spend hours 'babysitting' their pipeline jobs - has that been the case for you or your teams?
* Is your organization investing to improve CI/CD in the short term or long term? Is there a clearly defined strategy or timeline?
  * What’s the expectation on your team to support or facilitate this initiative?
  * Are you going to be onboarding additional teams in the next say, 12 months?
* Have your teams run into roadblocks or hurdles automating builds/tests at scale? Do you see any degradation in velocity as you scale?
* What is the workflow if a developer wants to create a new pipeline or add a job to an existing pipeline? How much time does that take the developer away from doing “real work”?

## Industry Analyst Resources

Examples of comparative research for this use case are listed just below. Additional research relevant to this use case can be found in the [Analyst Reports - Use Cases](https://docs.google.com/spreadsheets/d/1vXpniM08Ql0v0yDd22pcNmXpDrA-NInJOwj25PRuHXA/edit?usp=sharing) spreadsheet.

- [Forrester Wave for Cloud-Native CI Tools](/analysts/forrester-cloudci19/)
- [Forrester Continuous Integration Tools](/analysts/forrester-ci/)

### Industry Analyst Relations (IAR) Plan

- The IAR Handbook page has been updated to reflect our plans for [incorporating Use Cases into our analyst conversations](https://about.gitlab.com/handbook/marketing/product-marketing/analyst-relations/#how-we-incorporate-use-cases-into-our-analyst-conversations).
- For  details specific to each use case, and in respect of our contractual confidentiality agreements with Industry Analyst firms, our engagement plans are available to GitLab team members in the following protected document: [IAR Use Case Profile and Engagement Plan](https://docs.google.com/spreadsheets/d/14UthNcgQNlnNfTUGJadHQRNZ-IrAe6T7_o9zXnbu_bk/edit#gid=0).

For a list of analysts with a current understanding of GitLab's capabilities for this use case, please reach out to Analyst Relations via Slack (#analyst-relations) or by submitting an [issue](https://gitlab.com/gitlab-com/marketing/product-marketing/issues/new) and selecting the "AR-Analyst-Validation" template.

## Competitive Comparison
Amongst the many competitors in the DevOps space, [Jenkins](/devops-tools/jenkins-vs-gitlab.html) and [CircleCI](/devops-tools/circle-ci-vs-gitlab.html) are the closest competitors offering continuous integration capabilities.

## Proof Points - Customer Recognitions

### Quotes and reviews

#### Gartner Peer Insights

*Gartner Peer Insights reviews constitute the subjective opinions of individual end users based on their own experiences, and do not represent the views of Gartner or its affiliates. Obvious typos have been amended.*

>"We've migrated all our products from several "retired" VCS's to GitLab in only 6 months. - Including CI/CD process adoption - Without [loss] of code - Without frustrated developers - Without enormous costs"
>
> - **Application Development Manager**, [Gartner Peer Review](https://www.gartner.com/reviews/market/application-release-orchestration-solutions/vendor/gitlab/product/gitlab/review/view/1037269)
>
>"Finally, the most amazing thing about Gitlab is how well integrated the [GitLab] ecosystem is. It covers almost every step of development nicely, from the VCS, to CI, and deployment."
>
> - **Software Engineer**, [Gartner Peer Review](https://www.gartner.com/reviews/market/application-release-orchestration-solutions/vendor/gitlab/product/gitlab/review/view/1038051)
>
>"One of the best ways to approach source code management (Git) and release pipeline management [CI/CD]. [Gitlab] gives you a simple yet highly customizable approach to release management which is a complicated topic."
>
> - **System Engineer**, [Gartner Peer Review](https://www.gartner.com/reviews/market/application-release-orchestration-solutions/vendor/gitlab/product/gitlab/review/view/1045457)
>
>"The best software tool to manage all our projects and configure [CI/CD]. I will definitely recommend GitLab to everyone that wants to start a new project and doesn't want to use too many tools, GitLab has everything that you need to manage."
>
> - **Web Developer**, [Gartner Peer Review](https://www.gartner.com/reviews/market/application-release-orchestration-solutions/vendor/gitlab/product/gitlab/review/view/1063698)
>
>"Over the years, we have successfully taken advantage of [Gitlab's] continuous deployment and integration mechanisms to build [and] maintain a robust and solid codebase."
>
> - **Co-Founder/CEO**, [Gartner Peer Review](https://www.gartner.com/reviews/market/application-release-orchestration-solutions/vendor/gitlab/product/gitlab/review/view/1111080)
>
>"One of the best [tools] for continuous integration and continuous deployment. "
>
> - **Lead - Mobile Apps**, [Gartner Peer Review](https://www.gartner.com/reviews/market/application-release-orchestration-solutions/vendor/gitlab/product/gitlab/review/view/1117401)
>
>"Overall, the experience with Gitlab is very positive, it provides many powerful features especially regarding Continuous Integration and [Continuous Deployment]"
>
> - **Developer Analyst**, [Gartner Peer Review](https://www.gartner.com/reviews/market/application-release-orchestration-solutions/vendor/gitlab/product/gitlab/review/view/1140016)
>
>"[GitLab's] UI is so easy and manageable to understand. Built-in continuous integration is one of its best [features]."
>
> - **Technical Evangelist**, [Gartner Peer Review](https://www.gartner.com/reviews/market/application-release-orchestration-solutions/vendor/gitlab/product/gitlab/review/view/1092444)
>
>"GitLab isn't just for hosting your code, it's for the entire lifecycle of your code. Since they host code, it makes sense for them to provide services around development and getting code into production. Their integration into other services is really easy. They give you GitLab-CI for any CI/CD needs, driven from a yaml file."
>
> - **Testing Analyst**, [Gartner Peer Review](https://www.gartner.com/reviews/market/application-release-orchestration-solutions/vendor/gitlab/product/gitlab/review/view/886423)
>
>"This is really an amazing source code repository toolset which enables the robust CI practices which are inbuilt features of GitLab. This can be utilized well in any enterprise looking for the smooth CI/CD pipelines."
>
> - **Software Engineer**, [Gartner Peer Review](https://www.gartner.com/reviews/market/enterprise-agile-planning-tools/vendor/gitlab/product/gitlab/review/view/1009762)

### Blogs

#### [Jaguar Land Rover](/blog/2018/07/23/chris-hill-devops-enterprise-summit-talk/)

* **Problem:** JLR had **6 week feedback loops** resulting in slowdowns
* **Solution:** Premium (CI)
* **Result:** **Feedback loops reduced to 30 mins.** JLR is deploying within the engineering environment, 50-70 times per day of each individual piece of software to a target or to a vehicle.
* **Sales Segment:** Enterprise

#### [Ticketmaster](/blog/2017/06/07/continous-integration-ticketmaster/)

* **Problem:** Long Jenkins build times slowed down CI pipeline
* **Solution:** Premium (CI)
* **Result:** Less than 8 minutes total from commit to build, test and generate artifacts
* **Sales Segment:** Enterprise


### Case Studies

#### [Goldman Sachs](/customers/goldman-sachs/) 

* **Problem** Needed to increase developer efficiency and software quality 
* **Solution:** GitLab Premium (CI/CD, SCM) 
* **Result:** Improved from **1 build every two weeks to over a 1000/day**, or releasing 6 times per day per developer, and an average cycle time from branch to merge is now 30 minutes; simplified workflow and simplified administration
All the new strategic pieces of ’software development platforms are tied into GitLab. GitLab is used as a complete ecosystem for development, source code control and reviews, builds, testing, QA, and production deployments.
* **Sales Segment:** Enterprise

#### [CERN](/customers/cern/)

* **Problem:** CERN was looking for an open source way to host their pipelines
* **Solution:** GitLab Starter (CI)
* **Result:** The European-based particle physics laboratory, uses GitLab for more than **12,000 users and 120,000 CI jobs a month**
* **Sales Segment:** Enterprise

#### [Wish](https://about.gitlab.com/customers/wish/)

* **Problem:** Wish was using TeamCity for CI and build management, but their automated jobs kept failing. The development team used a homegrown toolchain that included GitHub for SCM and Phabricator for code reviews. 
* **Solution:** GitLab Premium (CI)
* **Result:** **50% reduction in pipeline down time.** Wish has removed the bottlenecks associated with complex toolchains.
* **Sales Segment:** Mid-Market

### References to help you close
[SFDC report of referenceable Verify customers](https://gitlab.my.salesforce.com/a6l4M000000kDwk) Note: Sales team members should have access to this report. If you do not have access, reach out to the [customer reference team](https://about.gitlab.com/handbook/marketing/product-marketing/customer-reference-program/#which-customer-reference-team-member-should-i-contact) for assistance.

Request reference calls by pressing the "Find Reference Accounts" button at the top of your stage 3 or later opportunity.

## Key Value (at tiers)

### Premium/Silver
- Describe the value proposition of why Premium/Silver for this Use Case

### Ultimate/Gold
- Describe the value proposition of why Ultimate/Gold for this Use Case

## Resources

### Presentations
* [Why CI/CD?](https://docs.google.com/presentation/d/1OGgk2Tcxbpl7DJaIOzCX4Vqg3dlwfELC3u2jEeCBbDk)

### Continuous Integration Videos
* [CI/CD with GitLab](https://youtu.be/1iXFbchozdY)
* [GitLab for complex CI/CD: Robust, visible pipelines](https://youtu.be/qy8A7Vp_7_8)

### Integrations Demo Videos
* [Migrating from Jenkins to GitLab](https://youtu.be/RlEVGOpYF5Y)
* [Using GitLab CI/CD pipelines with GitHub repositories](https://youtu.be/qgl3F2j-1cI)

### Clickthrough & Live Demos
* [Live Demo: GitLab CI Deep Dive](https://youtu.be/pBe4t1CD8Fc)
