---
layout: markdown_page
title: "Usecase: DevSecOps"
---

<!--

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}
-->

## DevSecOps

The DevSecOps usecase is applicable for customers who are trying to "shift left" to find security vulnerabilities earlier within their DevOps methodology but have not been able to achieve expected results.

Application Security is hard when security is separated from your DevOps flow. Security has traditionally been the final hurdle in the development life cycle. Iterative development workflows can make security a release bottleneck. Customers donn't have enough security people to test all of their code, and hiring more security analysts won't automatically reduce the friction between app sec and engineering teams. Only testing major releases, or limiting tests to certain apps, leaves weak spots hackers can exploit. They need a way to balance risk and business agility. Instead of waiting for security at the end of the development process, they want to include it within their DevOps workflow. Often this is referred to as DevSecOps.

DevSecOps integrates security controls and best practices in the DevOps workflow. DevSecOps automates security and compliance workflows to create an adaptable process for your development and security teams.

### Why is DevSecOps needed?

Balancing business velocity with security is possible. With GitLab, DevSecOps architecture is built into the CI/CD process. Every merge request is scanned through its pipeline for security issues and vulnerabilities in the code and its dependencies using automated tests. This enables some magic to happen.

### Benefits of DevSecOps

Every piece of code is tested upon commit for security threats, without incremental cost.
The developer can remediate now, while they are still working in that code, or create an issue with one click.
The dashboard for the security pro is a roll-up of vulnerabilities remaining that the developer did not resolve on their own.
Vulnerabilities can be efficiently captured as a by-product of software development.
A single tool also reduces cost over the approach to buy, integrate and maintain point solutions throughout the DevOps pipeline.

### Personas

[Who uses Secure capabilities](https://about.gitlab.com/handbook/marketing/product-marketing/competitive/application-security/#who-uses-gitlab-secure-capabilities)

[Market topology](https://about.gitlab.com/handbook/marketing/product-marketing/competitive/application-security/)

#### User Persona


#### Buyer Personas

### [Message House](./message-house/)

The message house for the use case provides a structure to describe and discuss the value and differentiators for the use case.

## Industry Analyst Coverage

Examples of comparative research for this use case are listed just below. Additional research relevant to this use case can be found in the [Analyst Reports - Use Cases](https://docs.google.com/spreadsheets/d/1vXpniM08Ql0v0yDd22pcNmXpDrA-NInJOwj25PRuHXA/edit#gid=0) spreadsheet.

- [Gartner - AppSec Testing Hype Cycle 2019](https://drive.google.com/file/d/1UOXO8YisjpBLFfdwQTDnJFhtjZf5LcJR/view?usp=sharing)
- [The Forrester Wave™: Software Composition Analysis, Q2 2019](https://drive.google.com/file/d/1gBJWRLRQv1NK1xXQQxvejo2hK7Y1TGgm/view?usp=sharing)

Please note that GitLab does NOT have reprint rights for these two reports, so please do not share these with customers, prospects, or partners.

### Industry Analyst Relations (IAR) Plan
- The IAR Handbook page has been updated to reflect our plans for [incorporating Use Cases into our analyst conversations](https://about.gitlab.com/handbook/marketing/product-marketing/analyst-relations/#how-we-incorporate-use-cases-into-our-analyst-conversations).
- For  details specific to each use case, and in respect of our contractual confidentiality agreements with Industry Analyst firms, our engagement plans are available to GitLab team members in the following protected document: [IAR Use Case Profile and Engagement Plan](https://docs.google.com/spreadsheets/d/14UthNcgQNlnNfTUGJadHQRNZ-IrAe6T7_o9zXnbu_bk/edit#gid=1124037301).

For a list of analysts with a current understanding of GitLab's capabilities for this use case, please reach out to Analyst Relations via Slack (#analyst-relations) or by submitting an [issue](https://gitlab.com/gitlab-com/marketing/product-marketing/issues/new) and selecting the "AR-Analyst-Validation" template.   


## Market Requirements (in priority order)

| Market Requirements | Description | Typical capability-enabling features | Value/ROI |
| ------ | ------ | ------ | ------ |
| Scan results within CI pipeline | For developers to find and fix vulnerabilities while they are coding, vulnerability scan results must be made available to the developer within their native workflow. Findings that can be automatically corrected should use automation to apply a fix and test the results. | Integrate scan results into the CI pipeline via CI scripting, APIs, Plug-ins or via a single application. Auto Remediation aims to automate vulnerability solution flow, and automatically create a fix, eliminating developer effort. The fix is then tested, and if it passes all the tests already defined for the application, it is deployed to production.  | Allows security flaws to be fixed early, when less expensive, removes context-switching, and minimizes risk by eliminating vulnerabilities. |
| Application Security Testing of code and components | Scanning the application code and components (aka Source Code Analysis/SCA). The GitLab survey shows Dependency scanning is most frequently used scan type. | SAST, Dependency, Container scanning, License Compliance, and Secrets Detection | Prevents creating new vulnerabilities.|
| Application Security Testing of running app | Looking for vulnerable code behavior as code executes. With growth in dependencies, there is increased need for discovery of this third party code which often requires a running app.| DAST, IAST, Input Fuzzing, UEBA/Threat modeling, third party code discovery, Mobile app sec testing | Prevents creating new vulnerabilities. |
| Security Governance | Security policies automated and integrated into the SDLC to consistently and proactively control or restrict software components that don't meet policy guidelines from entering production. | Security Policy Automation, Compliance Assessment, Security Risk Assessment, Audit Assessment, Security Process Improvement/ Assessment, Vulnerability Management, Vulnerability Database,  | Risk mitigation. Ability to identify exceptions and refine policies over time. |
| SecOps | Includes the security of the controls used to deploy the code. With DevOps, developers are more involved and desire closed-loop feedback from production to the developer. Emerging capabilities should allow the developer to set protections in production based upon things found in dev, and should monitor vulnerabilities and potential exploits of cloud-native app environments/configurations. | Runtime application shielding, App Infrastructure Protection for Containers, Kubernetes, APIs, and Serverless Security | Identify and remove risks from new attack surfaces introduced via cloud native. |
| Pre-CI/CD app sec | Tools that prevent the developer from crafting insecure code, before it is tested. | IDE Security guardrails (aka spell-check-like SAST) and pre-approved Bill of Materials | Prevents creating new vulnerabilities. |



## Top 3 Differentiators

| Differentiator | Value | Proof Point  |
|-----------------|-------------|---------------|
| **Scans performed on feature branch before code is merged & scan results in MR pipeline** | GitLab performs security scans like SAST, license compliance, dependency scanning before the code is merged - giving Developers opportunity to identify and fix security vulnerabilities before they context switch to other activities. This improves cycle time and development costs as the time and cost to resolve defects and vulnerabilities exponentially increase the later it is detected in the development cycle | [Gartner - Integrating Security Into the DevSecOps Toolchain](https://www.gartner.com/doc/3975263) explains how Security should be included in the DevSecOps lifecycle in small actionable steps that developers can take action on quickly & integrating into defect tracking workflow to match the pace of security fixes to the pace of development. |
| **Apply security policy & gating within the MR pipeline** | Bring Development and Security Teams closer by allowing security teams to apply organizational security policies before hand and review/approve security exceptions before the code is merged | **-**  |
| **Streamlined Auditing** | GitLab provides a single source of truth for Dev, Sec and Ops through a single data-store. Everything is audited and for every change, there is a single thread that contains the full audit log of every decision and action - making audit compliance a breeze | The auditor for [Glympse](/customers/glympse/) observed that the company had remediated security issues faster than any other company that he had worked with before in his 20-year career. Within one sprint, just 2 weeks, Glympse was able to implement security jobs across all of their repositories using GitLab’s CI templates |

### What Are The GitLab Advantages?

**Contextual**. Unlike traditional application security tools primarily intended for use by security pros, GitLab secure code capabilities are built into the CI/CD workflows where the developers live. We empower developers to identify vulnerabilities and remove them early in the development cycles. While at the same time, providing security professionals a dashboard to view items not already resolved by the developer, across projects. This contextual approach helps each role deal with items that are most important and most relevant to their scope of work within the delivery process.

**Congruent with DevOps processes**. GitLab secure capabilities support the decision-makers, within their natural workflow. Reports are interactive, actionable, and iterative and most important immediate and relevant to changes made. Developers immediately see the cause and affect of their own specific changes so they may iteratively address security flaws alongside code flaws.
Integrated with DevOps tools. When triaging vulnerabilities, users can confirm (creating an issue to solve the problem), or dismiss them (in case they are false positives or there are compensating controls). When using GitLab, no additional integration is needed between app sec and ticketing, CI/CD, etc.

**Efficient and automated**. Eliminates mundane work wherever possible. Auto remediation applies patches to vulnerable dependencies and even re-runs the pipeline to evaluate the viability of the patch.

## Competitive Comparison

See how we [compare against other Security tools](https://about.gitlab.com/devops-tools/)


## Proof Points - customers

### Quotes and reviews
<List of customer quotes/reviews from public sites>

### Customer Case Studies

* **[Glympse](/customers/glympse/)**
* **Problem** A complex developer tech stack with over 20 distinct tools that was hard to maintain and manage. Teams spent several hours a week keeping tools running rather than shipping innovation to their app.
* **Solution:** GitLab Ultimate (SCM, CI, DevSecOps) 
* **Results: ~20 tools consolidated into GitLab and remediated security issues faster than any other company in their Security Auditor's experience.
* **Sales Segment:** SMB

* **[BI Worldwide](https://about.gitlab.com/customers/bi_worldwide/)**
* **Problem:** Had a large legacy codebase that was created years ago. Looking to eliminate (or at least significantly reduce) manual testing and manual deployments to their on-premise infrastructure. 
* **Solution:** GitLab Ultimate (SCM, CI, DevSecOps)
* **Results:** **10x daily deployments** Simplified their toolchain and migrated from their previous Git tools to GitLab. Saw an immediate improvement of collaboration and release pace as a result of their move to GitLab.
* **Sales Segment:** Mid-Market

* **[Chorus](https://about.gitlab.com/customers/chorus/)**
* **Problem:** The founders of Chorus built the tool from the beginning using GitLab. 
* **Solution:** GitLab Ultimate (SCM, CI, DevSecOps)
* **Results:** **6 week production cycles reduced to 1 week** During a recent audit for SOC2 compliance, the auditors said that Chorus had the fastest auditing process they have seen and most of that is due to the capabilities of GitLab.
* **Sales Segment:** SMB

### References to help you close

[SFDC report of referenceable secure customers](https://gitlab.my.salesforce.com/a6l4M000000kDw2) Note: Sales team members should have access to this report. If you do not have access, reach out to the [customer reference team](https://about.gitlab.com/handbook/marketing/product-marketing/customer-reference-program/#which-customer-reference-team-member-should-i-contact) for assistance.

## Key Value (at tiers)

### Ultimate/Gold
- Describe the value proposition of why Ultimate/Gold for this Use Case

## Resources
### Presentations
* [Security customer presentation](https://docs.google.com/presentation/d/1WHTyUDOMuSVK9uK7hhSIQ_JbeUbo7k5AW3D6WwBReOg/edit?usp=sharing)

### White paper
* [A Seismic Shift Left](https://about.gitlab.com/resources/downloads/gitlab-seismic-shift-in-application-security-whitepaper.pdf)

### eBook: Ten Steps Every CISO Should Take to Secure Next-gen Applications
* [Gated](https://lnkd.in/er8tjQg)
* [Ungated](https://drive.google.com/file/d/0B-ZpQfvLs-2AVFI5VmNybTBvWWttRWxENWpGVnlNbVBFODNZ/view?usp=sharing)

### Integration with third party commercial scanners
* [WhiteSource](https://about.gitlab.com/handbook/marketing/product-marketing/enablement/security-integrations-whitesource/)

### Simplifying DevOps Videos
* [Security Dashboard demo](https://youtu.be/U2_dqwTRUVk)
* [Deep Dive into a Security demo](https://youtu.be/k4vEJnGYy84)


### Integrations Demo Videos
* See how [integration is the key to successful DevSecOps](https://about.gitlab.com/blog/2018/09/11/what-south-africa-taught-me-about-cybersecurity/)

### Clickthrough & Live Demos
* [All Marketing Click Through Demos](/handbook/marketing/product-marketing/demo/#click-throughs)
* [All Marketing Live Demos](/handbook/marketing/product-marketing/demo/#live-instructions)
